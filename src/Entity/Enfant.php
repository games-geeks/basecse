<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use App\Repository\EnfantRepository;

#[ORM\Entity(repositoryClass: EnfantRepository::class)]
class Enfant
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'string', length: 255)]
    private $prenom;

    #[ORM\Column(type: 'datetime')]
    private $dateNaissance;

    #[ORM\ManyToOne(targetEntity: Salarie::class, inversedBy: 'enfants')]
    private $parent;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getParent(): ?Salarie
    {
        return $this->parent;
    }

    public function setParent(?Salarie $parent): self
    {
        $this->parent = $parent;

        return $this;
    }


    public function getAge(): int
    {
        //date in mm/dd/yyyy format; or it can be in other formats as well
        $birthDate = $this->getDateNaissance()->format('d/m/Y');;

        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return  $age;

        // $currentDate = date("d-m-Y");
        // $age = date_diff(date_create($this->getDateNaissance), date_create($currentDate));
        // return $age->format("%y");
    }

    public function __toString()
    {
        return $this->prenom . ' ' . $this->nom;
    }
}
