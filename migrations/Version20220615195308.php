<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220615195308 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE montant_carte (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, niveau_scolarite VARCHAR(255) NOT NULL, montant INTEGER NOT NULL)');
        $this->addSql('DROP INDEX IDX_34B70CA2727ACA70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__enfant AS SELECT id, parent_id, nom, prenom, date_naissance FROM enfant');
        $this->addSql('DROP TABLE enfant');
        $this->addSql('CREATE TABLE enfant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, parent_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, CONSTRAINT FK_34B70CA2727ACA70 FOREIGN KEY (parent_id) REFERENCES salarie (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO enfant (id, parent_id, nom, prenom, date_naissance) SELECT id, parent_id, nom, prenom, date_naissance FROM __temp__enfant');
        $this->addSql('DROP TABLE __temp__enfant');
        $this->addSql('CREATE INDEX IDX_34B70CA2727ACA70 ON enfant (parent_id)');
        $this->addSql('DROP INDEX IDX_828E3A1AF6BD1646');
        $this->addSql('CREATE TEMPORARY TABLE __temp__salarie AS SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM salarie');
        $this->addSql('DROP TABLE salarie');
        $this->addSql('CREATE TABLE salarie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, code_agent VARCHAR(20) NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, CONSTRAINT FK_828E3A1AF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO salarie (id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at) SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM __temp__salarie');
        $this->addSql('DROP TABLE __temp__salarie');
        $this->addSql('CREATE INDEX IDX_828E3A1AF6BD1646 ON salarie (site_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE montant_carte');
        $this->addSql('DROP INDEX IDX_34B70CA2727ACA70');
        $this->addSql('CREATE TEMPORARY TABLE __temp__enfant AS SELECT id, parent_id, nom, prenom, date_naissance FROM enfant');
        $this->addSql('DROP TABLE enfant');
        $this->addSql('CREATE TABLE enfant (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, parent_id INTEGER DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL)');
        $this->addSql('INSERT INTO enfant (id, parent_id, nom, prenom, date_naissance) SELECT id, parent_id, nom, prenom, date_naissance FROM __temp__enfant');
        $this->addSql('DROP TABLE __temp__enfant');
        $this->addSql('CREATE INDEX IDX_34B70CA2727ACA70 ON enfant (parent_id)');
        $this->addSql('DROP INDEX IDX_828E3A1AF6BD1646');
        $this->addSql('CREATE TEMPORARY TABLE __temp__salarie AS SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM salarie');
        $this->addSql('DROP TABLE salarie');
        $this->addSql('CREATE TABLE salarie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, code_agent VARCHAR(20) NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL)');
        $this->addSql('INSERT INTO salarie (id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at) SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM __temp__salarie');
        $this->addSql('DROP TABLE __temp__salarie');
        $this->addSql('CREATE INDEX IDX_828E3A1AF6BD1646 ON salarie (site_id)');
    }
}
