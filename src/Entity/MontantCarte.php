<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MontantCarteRepository;


#[ORM\Entity(repositoryClass: MontantCarteRepository::class)]
class MontantCarte
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $niveauScolarite;

    #[ORM\Column(type: 'integer')]
    private $montant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveauScolarite(): ?string
    {
        return $this->niveauScolarite;
    }

    public function setNiveauScolarite(string $niveauScolarite): self
    {
        $this->niveauScolarite = $niveauScolarite;

        return $this;
    }

    public function getMontant(): ?int
    {
        return $this->montant;
    }

    public function setMontant(int $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getMontantAdmin(): ?int
    {
        return $this->montant * 100;
    }
}
