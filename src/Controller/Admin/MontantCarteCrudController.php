<?php

namespace App\Controller\Admin;

use App\Entity\MontantCarte;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MontantCarteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MontantCarte::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('niveauScolarite', 'niveau'),
            MoneyField::new('montant', 'Montant')->setCurrency('EUR'),
        ];
    }
}
