<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220615162423 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_828E3A1AF6BD1646');
        $this->addSql('CREATE TEMPORARY TABLE __temp__salarie AS SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM salarie');
        $this->addSql('DROP TABLE salarie');
        $this->addSql('CREATE TABLE salarie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, code_agent VARCHAR(20) NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, CONSTRAINT FK_828E3A1AF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO salarie (id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at) SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM __temp__salarie');
        $this->addSql('DROP TABLE __temp__salarie');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_828E3A1AF6BD1646 ON salarie (site_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_828E3A1AF6BD1646');
        $this->addSql('CREATE TEMPORARY TABLE __temp__salarie AS SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM salarie');
        $this->addSql('DROP TABLE salarie');
        $this->addSql('CREATE TABLE salarie (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, site_id INTEGER DEFAULT NULL, code_agent VARCHAR(20) NOT NULL, prenom VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, date_embauche DATE DEFAULT NULL, created_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , updated_at DATETIME NOT NULL --(DC2Type:datetime_immutable)
        )');
        $this->addSql('INSERT INTO salarie (id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at) SELECT id, site_id, code_agent, prenom, nom, date_embauche, created_at, updated_at FROM __temp__salarie');
        $this->addSql('DROP TABLE __temp__salarie');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_828E3A1AF6BD1646 ON salarie (site_id)');
    }
}
