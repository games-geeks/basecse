<?php

namespace App\Controller\Admin;

use App\Entity\Salarie;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SalarieCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Salarie::class;
    }


    public function configureFields(string $pageName): iterable
    {

        return [
            TextField::new('codeAgent', 'Matricule'),
            TextField::new('prenom', 'Prénom'),
            TextField::new('nom', 'Nom'),
            AssociationField::new('site'),
            AssociationField::new('enfants'),
            DateField::new('dateEmbauche', 'Date Embauche'),

        ];
    }
}
