<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use App\Repository\SalarieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SalarieController extends AbstractController
{
    #[Route('/salarie', name: 'app_salarie')]
    public function index(PaginatorInterface $paginator, SalarieRepository $repo, Request $request): Response
    {

        $salaries = $repo->findBy([], ['nom' => 'ASC']);

        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $salaries,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            52
        );

        $articles->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);



        return $this->render('salarie/index.html.twig', [
            'controller_name' => 'SalarieController',
            'pagination' => $articles,
        ]);
    }

    #[Route('/pdf', name: 'pdf_salarie')]
    public function pdf(PaginatorInterface $paginator, SalarieRepository $repo, Request $request)
    {
        $salaries = $repo->findBy([], ['nom' => 'ASC']);

        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $salaries,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            52
        );

        $articles->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);

        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->render('salarie/index.html.twig', [
            'controller_name' => 'SalarieController',
            'pagination' => $articles,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => false
        ]);
    }
}
