<?php

namespace App\Controller\Admin;

use App\Entity\Enfant;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EnfantCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Enfant::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('prenom', 'Prénom'),
            TextField::new('nom', 'Nom'),
            IntegerField::new('age', 'Age'),
            AssociationField::new('parent'),
            DateField::new('dateNaissance', 'Date Naissance'),
        ];
    }
}
